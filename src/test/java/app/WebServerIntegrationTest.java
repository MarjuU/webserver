package app;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import static org.mockito.Mockito.spy;


public class WebServerIntegrationTest {

    @Test
    public void testServer() throws UnknownHostException {
        final Runnable startServerRunnable = () -> {
            System.out.println("Starting server");
            WebServer testingServer = new WebServer(4444);
            testingServer.addPage("/Integration", "integreerume");
            testingServer.start();
        };

        final Thread serverThread = new Thread(startServerRunnable);

        serverThread.start();

        try {
            System.out.println("start connecting to server");

            Socket socket = new Socket("localhost", 4444);

            System.out.println("Connecting to server");

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);

            writer.println("GET /Integration HTTP/1.1\n"
                    + "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0\n"
                    + "Host: localhost:1234\n"
                    + "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n"
                    + "Accept-Language: en-US,en;q=0.5\n"
                    + "Accept-Encoding: gzip, deflate\n"
                    + "Connection: keep-alive\n"
                    + "\n"
            );

            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));





        } catch (Exception e) {
            System.out.println("Server exception: " + e.getMessage());
            e.printStackTrace();
        }

    }

}