package app;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static org.mockito.Mockito.spy;


public class WebServerTest {
    WebServer testServer = spy(new WebServer(4444));

    @Test
    public void testresponseMaker() {

        String message = "sonum";
        Mockito.doReturn(ZonedDateTime.parse("2019-09-30T10:15:30+01:00[Europe/Helsinki]")).when(testServer).now();
        String result = testServer.responseMaker(message);
        Assert.assertEquals("HTTP/1.1 200 OK\n" +
                "Date: 2019-09-30T10:15:30+03:00[Europe/Helsinki]\n" +
                "Server: Marju server 1.0\n" +
                "Content-Length: 5\n" +
                "Content-Type: text/html \n" +
                "\n" +
                "sonum", result);
    }

    @Test
    public void testrequestBodyMap() {
        ArrayList<String> requestStrings = new ArrayList<>();
        requestStrings.add("HTTP/1.1 200 OK");
        requestStrings.add("\"Server: Marju server 1.0");
        requestStrings.add("\"Server: Marju server 1.0");
        requestStrings.add("firstname=marju&lastname=uusen");

        HashMap<String, String> testMap = new HashMap<>();
        testMap.put("firstname", "marju");
        testMap.put("lastname", "uusen");

        HashMap<String, String> resultMap = new HashMap<String, String>(testServer.requestBodyMap(requestStrings));

        Assert.assertEquals("size mismatch for maps", testMap.size(), resultMap.size());
        Assert.assertTrue("Missing keys in resulting map", resultMap.keySet().containsAll(testMap.keySet()));
        testMap.keySet().stream().forEach((key) -> {
            Assert.assertEquals("Value mismatch for key '" + key + "';", testMap.get(key), resultMap.get(key));
        });
    }
}