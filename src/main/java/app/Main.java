package app;

public class Main {
    public static void main(String[] args) {

        WebServer myServer = new WebServer(4444);

        /*Function<Map<String, String>, String> formHandler = (postBody -> {
            String firstName = postBody.get("firstname");
            String lastName = postBody.get("lastname");
            return "Teretulemast koju " + firstName + " " + lastName;
        });*/

        myServer.addPage("/", "see on lyhikese aadressiga leht");
        myServer.addPage("/test", "siia kirjutame testide tulemused");


        myServer.addFile("/Kookos", "20190803_103516.jpg");
        myServer.addFile("/Hobune", "20190907_115858.jpg");
        myServer.addFile("/source.gif", "source.gif");
        myServer.addFile("/favicon.ico", "icon.png");
        myServer.addFile("/Nimevorm", "nimevorm.html");
        myServer.addFile("/Koduaken", "Koduaken.html");

        myServer.addForm("/Nimevorm1", "FormReply.html");

        myServer.addRedirect("/Lemmikloom", "/Hobune");
        myServer.addRedirect("/Otsing", "http://www.google.com");


        myServer.start();

    }

}
