package app;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;


public class WebServer {
    //variables that each server has.
    // 1. Port nr
    private int port;
    //2. list of page paths and messages to display
    private HashMap<String, String> pages = new HashMap<String, String>();
    //3. list of page paths and (image or html etc) files to use in response content
    private HashMap<String, String> files = new HashMap<String, String>();
    //4. list of forms with the respective funtions
   // private  HashMap<String, Function<Map<String, String>, String>> forms = new HashMap<>();
    private HashMap<String, String> forms = new HashMap<>();
    //5. list of redirects
    private  HashMap<String, String> redirects = new HashMap<>();


    //konstruktor
    public WebServer(int port) {
        this.port = port;
    }

    //veebiserveri kaivitamise meetod
    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started \nListening connections on port: " + port);

            while(true) {
                //create outputstream outside lower try catch block to be able to send 500 server error page in the finally block
                Socket socket = socket = serverSocket.accept();
                OutputStream output = socket.getOutputStream();
                DataOutputStream outputSecond =
                        new DataOutputStream(socket.getOutputStream());

                PrintWriter writer = new PrintWriter(outputSecond, true);

                try {
                    System.out.println("New client connected");

                    //connection established, set in and out movements
                    //in
                    InputStream input = socket.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader((input)));

                    //get request as array of strings line by line
                    ArrayList<String> requestStrings = requestIntoString(reader);

                    //print request onto console
                    stringPrinter(requestStrings);
                    System.out.println("siin on request juba labi kainud");

                    //store request first line in string array to be able to access the path segment of it
                    String[] requestFirstLine = new String[3];

                    if (requestStrings.size() > 0) {
                        requestFirstLine = requestStrings.get(0).split(" ");
                    }


                    //assemble http responces according to page path
                    //first check if path exist in hashmap referencing text for response
                    if (pages.containsKey(requestFirstLine[1])) {
                        writer.println(responseMaker(pages.get(requestFirstLine[1])));
                    }

                    //if the path refers to page with picture, use other responseMaker to send bytes
                    else if (files.containsKey(requestFirstLine[1])) {
                        responseMakerBytes(files.get(requestFirstLine[1]), outputSecond);
                    }

                    //if path is in hashmap for forms, assemble the response using formhandler function
                    else if (forms.containsKey(requestFirstLine[1])) {
                        MustacheFactory mf = new DefaultMustacheFactory();
                        Mustache mustache = mf.compile("FormReply.html");

                        StringWriter vuntsWriter = new StringWriter();
                        mustache.execute(vuntsWriter, requestBodyMap(requestStrings));

                        String mustacheString = vuntsWriter.toString();

                        //Function<Map<String, String>, String> formHandler = forms.get(requestFirstLine[1]);

                        //final String formHandlerApply = formHandler.apply(requestBodyMap(requestStrings));

                        writer.println(responseMaker(mustacheString));

                    }

                    //if path is referencing to redirect, use responsemaker for redirect
                    else if (redirects.containsKey(requestFirstLine[1])) {
                        writer.println(responseMakerRedirect(requestFirstLine[1]));
                    }

                    //return 404 not found if page path is not found
                    else {
                        writer.println(responseMaker404("<h2><font color=\"red\" face =\"verdana\">404 Not found </font> </h2>Page not found"));
                    }


                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }

                finally {
                    //if server throws and exception in the while loop, this will close connection and send response with 500 error
                    writer.println("HTTP/1.1 500 Server error\n" +
                            "Date: " + now().toString() + "\n" +
                            "Server: Marju server 1.0\n" +
                            "Content-Length: 57\n" +
                            "Content-Type: text/html \n" +
                            "\n" +
                            "<h2>500 server error</h2> <p>Please go back and retry</p>");
                    socket.close();
                }
            }
        }

        catch(IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    //method to create http responses for text outputs
    public String responseMaker (String message) {
        return "HTTP/1.1 200 OK\n" +
                "Date: " + now().toString() + "\n" +
                "Server: Marju server 1.0\n" +
                "Content-Length: " + message.length() + "\n" +
                "Content-Type: text/html \n" +
                "\n" +
                message;

    }

    // create time in separate method for method testing
    public ZonedDateTime now() {
        return ZonedDateTime.now();
    }

    //create http response as bytes (to send files) https://www.net.t-labs.tu-berlin.de/teaching/computer_networking/02.08.htm
    public void responseMakerBytes (String fileName, DataOutputStream outputSecond) throws IOException {
        //create file object from picture stored in resources folder
        try {

            //make the file into byte array


            byte[] data = IOUtils.toByteArray(Main.class.getClassLoader().getResourceAsStream(fileName));

                    /*Files.readAllBytes(Paths.get(Main.class.getClassLoader().getResource(fileName).toURI()));*/

            //get the length of the byte array
            int numOfBytes = (int) data.length;


            //write header into output as bytes
            outputSecond.writeBytes("HTTP/1.1 200 OK\n");
            if(fileName.contains(".jpg")) {
                outputSecond.writeBytes("Content-Type: image/jpg\n");

            }
            if(fileName.contains(".gif")) {
                outputSecond.writeBytes("Content-Type: image/gif\n");

            }
            if(fileName.contains(".png")) {
                outputSecond.writeBytes("Content-Type: image/png\n");

            }
            outputSecond.writeBytes("Content-Length: " + numOfBytes + "\n");
            outputSecond.writeBytes("\n");

            //write bytearray (filled with data from file into output as bytes
            outputSecond.write(data, 0, numOfBytes);

        }

        catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }

    }

    //create response for redirected pages
    public String responseMakerRedirect (String oldPath){
        return "HTTP/1.1 301 Moved Permanently\n" +
                "Location: " + redirects.get(oldPath) + "\n" +
                "Date: " + now().toString() + "\n" +
                "Server: Marju server 1.0\n" +
                "Content-Length: 0\n" +
                "Content-Type: text/html \n" +
                "Connection: close" +
                "\n" +
                "tekst";

    }

    public String responseMaker404 (String message) {
        return "HTTP/1.1 404 Not found\n" +
                "Date: " + now().toString() + "\n" +
                "Server: Marju server 1.0\n" +
                "Content-Length: " + message.length() + "\n" +
                "Content-Type: text/html \n" +
                "\n" +
                message;
    }

    //create string array from inputstream (request)
    public ArrayList<String> requestIntoString(BufferedReader reader) {
        ArrayList<String> requestStrings = new ArrayList<>();
        HashMap<String, String> headers = new HashMap<>();

        try {
            //store first word of first line as method
            String firstLine = reader.readLine();

            if (firstLine != null) {


                String[] firstLineSplitted = new String[3];
                firstLineSplitted = firstLine.split(" ");
                String method = firstLineSplitted[0];
                requestStrings.add(firstLine);

                while (true) {
                    String line = reader.readLine();

                    //different actions after emtpy line for post and get methods
                    if (line.equals("")) {
                        if (method.equals("GET")) {
                            requestStrings.add("");
                            break;
                        }
                        if (method.equals("POST")) {
                            requestStrings.add("");
                            char[] requestBody = new char[Integer.parseInt(headers.get("Content-Length"))];
                            reader.read(requestBody, 0, Integer.parseInt(headers.get("Content-Length")));
                            requestStrings.add(new String(requestBody));
                            break;
                        }
                    }


                    //insert each header into hashmap (to be able to access values later (eg for content-length)
                    String[] lineSplitted = line.split(": ");
                    headers.put(lineSplitted[0], lineSplitted[1]);

                    //inserts all nonblank lines into the array
                    requestStrings.add(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return requestStrings;
    }

    //put form fields data from request body into a Map structure. field name as key and value as value.
    public HashMap<String, String> requestBodyMap (ArrayList<String> requestStrings) {
        HashMap<String, String> requestBodyMap = new HashMap<>();

        String requestBody = requestStrings.get(requestStrings.size() - 1);
        String[] formFields = requestBody.split("&");

        for(String field : formFields) {
            String[] fields = field.split("=");
            requestBodyMap.put(fields[0], fields[1]);
        }
        return requestBodyMap;
    }

    //print string array on console
    public void stringPrinter (ArrayList<String> strings) {
        for (String s : strings) {
            System.out.println(s);
        }
    }

    public void addPage (String page, String message) {
        this.pages.put(page, message);
    }

    public void removePage(String path) {
        this.pages.remove(path);
    }

    public void addFile (String filename, String fileLocation) {
        this.files.put(filename, fileLocation);
    }

    public void addForm (String path, String formHtml) {
        this.forms.put(path, formHtml);
    }

    public void addRedirect(String pathOriginal, String pathNew) {
        this.redirects.put(pathOriginal, pathNew);
    }

}
